<?php
include('pping.php');

//Nouvelle instance de la class pping
$ping_test = new pping($argc, $argv);

//On lance le test
if ($ping_test->run())
{
	echo("Le test s'est déroulé avec succès" . PHP_EOL);
}
else
{
	echo("Le test a échoué" . PHP_EOL);
}