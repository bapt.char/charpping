<?php
include('config.php');
include('constants.php');

class pping
{
	private string $path_ip_list;  //Chemin du fichier contenant les addresses
	private string $path_output;   //Chemin du fichier de sortie
	private int    $argc;          //Nombre de paramètres
	private array  $argv;          //Tableau des valeurs des paramètres
	private array  $list;          //List des addresses IP

    /**
     * Contructeur de la class
     * @param int       $argc     Nombre d'arguments contenu dans $argv
     * @param array     $argv     Tableau contenant les paramètres
     */
    public function __construct($argc, $argv)
    {
    	$this->argc = $argc;
    	$this->argv = $argv;
        //$this->ptext("Initialised!");
    }

    /**
     * Lance le test IP
     * @return bool Booléen indiquant la réeussite du test
     */
    public function run() : bool
    {
        //On vérifié les paramètres d'entrées
        $status = $this->check_args();

        //On affiche l'érreur correspante (cas échéant)
        if ($status == STATUS_INVALID_ARGS_COUNT)
        {
            //Le nombre d'arguments n'est pas valide donc les paramètrs sont incorrects
            //On affiche les conseils d'usage
            $this->print_usage();
            return false;
        }
        else if ($status == STATUS_FILE_NOT_EXISTS)
        {
            //Le fichier d'entrée n'existe pas
            //On affiche un message d'erreur en mentionant le chemin invalide
            $this->perror("Le fichier '" . $this->path_ip_list . "'' n'existe pas");
            return false;
        }

        //On lis le fichier contenant les addresses à tester
    	$this->list = file($this->path_ip_list);

        //Fichier non valide
    	if (!$this->list)
    	{
            //Affichage d'érreur
    		$this->perror("Le fichier '" . $this->path_ip_list . "' ne peut être lu");
    		return false;
    	}

        //Nettoyage de la liste d'addresses (caractères inutiles avant/après)
    	$this->list = $this->trim_string_array($this->list);
    	//var_dump($this->list);

        $invalid_ip_list = array();

        //Vérification de la validité des addresses
    	if (!$this->check_list($this->list, $invalid_ip_list))
    	{
            //Affichage des addresses invalides
            foreach($invalid_ip_list as $e)
            {
                $this->perror("L'addresse IP '" . $e['ip'] . "' n'est pas valide (ligne ".($e['line']).")");
            }

    		return false;
    	}

    	$this->ptext("Lancement des tests pour " . count($this->list) . " addresses IP");

        $html_content = array();

        //On itère la liste des addresses
        foreach ($this->list as $key => $ip) 
        {
            //On éffectue le test
            $result = $this->is_ip_reachable($ip);

            $result_str = ($result ? 'OK' : 'FAIL');

            //On ajoute le code html correspondant à une ligne du tableau
            //dans le tableau $html_content
            array_push($html_content, "<tr><td>$ip</td><td>$result_str</td></tr>");

            //On affiche le résultat
            $this->ptext("[$ip] $result_str");
        }

        //On va chercher la date actuelle
        $date = date('d-m-Y à H:i');

        //On prends le contenu du fichier 'prototype'
        $html = file('default.html');

        for ($i = 0; $i < count($html); $i++)
        {
            if ($this->str_contains($html[$i], DATE_HERE))
            {
                $html[$i] = str_replace(DATE_HERE, "<p>Généré le $date</p>", $html[$i]);
            }
            else if ($this->str_contains($html[$i], CONTENT_HERE))
            {
                $html[$i] = str_replace(CONTENT_HERE, implode($html_content), $html[$i]);
            }
        }

        //On ouvre le fichier
        $file = fopen($this->path_output, 'w');

        //On écris dans le fichier
        fwrite($file, implode($html));

        //Fermeture du fichier
        fclose($file);

    	return true;
	}

    /**
     * On vérifie sur une chaine contiens une aute
     * @param   string  $a  Première chaîne à comparer
     * @param   string  $b  Seconde chaîne à comarer
     * @return  bool        Indique si la chaine $a contiens la chaine $b
     */
    private function str_contains(string $a, string $b)
    {
        return !(strpos($a, $b) === false);
    }

    /**
     * Vérifie si une addresse IP est joignable
     * @param string    $ip     Addresse IP à tester
     * @param int       $count  (optionnel) Nombre de requêtes à envoyer (par défaut: 1)
     */
    private function is_ip_reachable(string $ip, int $count = 1) : bool
    {
        $result = exec("ping -c $count $ip", $output, $status);

        //echo($outcome . PHP_EOL);
        //var_dump($outcome);

        return $status == STATUS_SUCCESS;
    }

    /**
     * Nettoie un tableau de chaînes de caractères (caractères inutile avant et après)
     * @param   array     $array    Tableau d'entrée
     * @return  array               Tableau de sortie
     */
	private function trim_string_array(array $array) : array
	{
		foreach ($array as $i => $element)
		{
			$array[$i] = trim($element);
		}

		return $array;
	}

    /**
     * Vérifie les paramètres d'entrée
     * @return  int     Code de retour indiquant si une erreur est survenue
     */
	private function check_args() : int
    {
        //Vérification du nombre d'arguments
    	if ($this->argc != ARGS_COUNT)
    	{
    		//Nombre d'arguments non valide
    		return STATUS_INVALID_ARGS_COUNT;
    	}

        //Le chemin vers le fichier de la liste des IP 
        //est le deuxième élément du tableau argv
    	$this->path_ip_list = $this->argv[1];
        
        //Le chemin vers le fichier de sortie
        //est le troisième élément du tableau argv
    	$this->path_output = $this->argv[2];

        //Vérification du chemin/fichié spécifié
    	if (!is_file($this->path_ip_list))
    	{
            //Le fichier n'éxiste pas ou n'est pas valide
    		return STATUS_FILE_NOT_EXISTS;
    	}

        //Les paramètres sont valides
    	return STATUS_SUCCESS;
    }

    /**
     * Affiche une erreur
     * @param   string  $trace      Message d'erreur à afficher
     * @param   bool    $newline    (optionnel) Défini si l'on passe une ligne à la fin du message
     */
    private function perror(string $trace, bool $newline = true) : void
    {
    	$this->ptext(ERROR . $trace, $newline);
    }

    /**
     * Affiche une message
     * @param   string  $text       Message à afficher
     * @param   bool    $newline    (optionnel) Défini si l'on passe une ligne à la fin du message
     */
    private function ptext(string $text, bool $newline = true) : void
    {
    	echo(PREFIX . $text . ($newline ? PHP_EOL : ''));
    }

    /**
     * Affiche les indications concernant l'usage du script
     */
    private function print_usage() : void
    {
    	$this->perror("Usage incorrect (example: script.php <liste_ips.txt> <sortie.html>)");
      	$this->perror("'liste_ips.txt': liste des addresses IP à tester (une par ligne)");
      	$this->perror("'sortie.html': fichier qui contiendra les résultats à la fin du test");
    }

    /**
     * Vérifie qu'un tableau d'addresses IP est valide c'est à dire 
     * que toutes les addresses contenues à l'interieur semblent correctes
     * @param  array    $list               Liste d'entrée contenant les addresses à vérifier
     * @param  array    &$invalid_ip_list   Référence vers un tableau (addresses non valides)
     * @return bool     Indique si toutes les addresses sont valides
     */
    private function check_list(array $list, array &$invalid_ip_list) : bool
    {
        foreach($list as $i => $ip)
        {	    
	        if (!filter_var($ip, FILTER_VALIDATE_IP))
	        {
                array_push($invalid_ip_list, array(
                    'line' => $i + 1,
                    'ip' => $ip
                ));
	        }   
        }
    
        return count($invalid_ip_list) == 0;
    }
}
