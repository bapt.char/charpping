<?php

/* CODES DE RETOUR */

define("STATUS_INVALID_ARGS_COUNT", 0x1337);     //Nombre d'arguments non conforme
define("STATUS_FILE_NOT_EXISTS", 0x1338);        //Le fichier n'existe pas
define("STATUS_SUCCESS", 0x0);                   //Succès 

/* CONTENU DE REMPLACEMENT */

define("DATE_HERE", "<!-- DATE_HERE -->");
define("CONTENT_HERE", "<!-- CONTENT_HERE -->");